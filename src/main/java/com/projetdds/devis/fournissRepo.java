package com.projetdds.devis;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface fournissRepo extends CrudRepository<fournisseur, Long> {
    List<fournisseur> findByAddress(String a);

}
