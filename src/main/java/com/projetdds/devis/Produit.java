package com.projetdds.devis;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class Produit {

    @Id
    public Long id_prod;
    public String designation;
    public int quantity;
    @ManyToOne
    public fournisseur fournisseur;

}
