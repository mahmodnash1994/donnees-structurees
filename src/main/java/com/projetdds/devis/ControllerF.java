package com.projetdds.devis;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

@Controller
public class ControllerF {

    @Inject
    fournissRepo fournisseurRepo;

    @GetMapping("/")
    public String afficher() {
        return "pageRecherche";
    }

    public String qst;

    @PostMapping("/generateq")
    public String generateQuestion(Question ff) {
        qst = ff.getInfo();
        String qstt = ff.getTypeQst();
        System.out.println("########################" + qst);
        System.out.println("########################" + qstt);
        if (qstt.equals("Les fournisseurs ont le produit")) {
            try {

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

                // élément de racine
                Document doc = docBuilder.newDocument();

                // l'élément contact
                Element contact = doc.createElement("question");
                doc.appendChild(contact);

                // attributs de l'élément contact
                Attr attr = doc.createAttribute("id");
                attr.setValue("1");
                contact.setAttributeNode(attr);

                // le nom
                Element type = doc.createElement("TypeMessage");
                type.appendChild(doc.createTextNode((String) qstt));
                contact.appendChild(type);

                // le nom
                Element nom = doc.createElement("information");
                nom.appendChild(doc.createTextNode((String) qst));
                contact.appendChild(nom);

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult resultat = new StreamResult(new File("question1.xml"));

                transformer.transform(source, resultat);

                System.out.println("Fichier sauvegardé avec succès!");

            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerException tfe) {
                tfe.printStackTrace();
            }
            return "redirect:/fournisseurs";
        } else {
            try {

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

                // élément de racine
                Document doc = docBuilder.newDocument();

                // l'élément contact
                Element contact = doc.createElement("question");
                doc.appendChild(contact);

                // attributs de l'élément contact
                Attr attr = doc.createAttribute("id");
                attr.setValue("2");
                contact.setAttributeNode(attr);

                Element type = doc.createElement("TypeMessage");
                type.appendChild(doc.createTextNode((String) qstt));
                contact.appendChild(type);

                // le nom
                Element nom = doc.createElement("information2");
                nom.appendChild(doc.createTextNode((String) qst));
                contact.appendChild(nom);

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult resultat = new StreamResult(new File("question2.xml"));

                transformer.transform(source, resultat);

                System.out.println("Fichier sauvegardé avec succès!");

            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerException tfe) {
                tfe.printStackTrace();
            }

            return "redirect:/fournisseurs";
        }

    }

    @GetMapping("/fournisseurs")
    public String recupFournisseur(Model model) {
        // fournisseurRepo.save()

        List<fournisseur> aa = (List<fournisseur>) fournisseurRepo.findByAddress(qst);
        model.addAttribute("fournisseur", aa);
        System.out.println(aa);
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            // élément de racine
            Document doc = docBuilder.newDocument();

            // l'élément contact
            Element contact = doc.createElement("reponse");
            doc.appendChild(contact);

            // attributs de l'élément contact
            Attr attr = doc.createAttribute("id");
            attr.setValue("1");
            contact.setAttributeNode(attr);

            for (int i = 0; i < aa.size(); i++) {
                // le nom
                Element nom = doc.createElement("fournisseur");
                nom.appendChild(doc.createTextNode((String) aa.get(i).getFirstName()));
                contact.appendChild(nom);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult resultat = new StreamResult(new File("reponse1.xml"));

            transformer.transform(source, resultat);

            System.out.println("Fichier sauvegardé avec succès!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

        return "pageResultat";
    }

}
