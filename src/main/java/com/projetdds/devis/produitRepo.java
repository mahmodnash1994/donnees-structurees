package com.projetdds.devis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface produitRepo extends CrudRepository<Produit, Long> {

}
