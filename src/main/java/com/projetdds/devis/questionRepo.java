package com.projetdds.devis;

import org.springframework.data.repository.CrudRepository;

public interface questionRepo extends CrudRepository<Question, Long> {

}
