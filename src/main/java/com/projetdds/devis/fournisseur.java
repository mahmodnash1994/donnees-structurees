package com.projetdds.devis;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class fournisseur {

    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id_four;
    public String firstName;
    public String lastName;
    public String address;
    @OneToMany(mappedBy = "id_prod")
    public List<Produit> produits;

}
